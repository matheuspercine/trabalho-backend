const User = require('../src/models/User');

const index = async(req,res) => {
    try {
        const users = await User.findAll({
            include: [
                'selling',
                'sold'
            ]
        });
        return res.status(200).json({users});
    }
    catch(err) {
        return res.status(500).json({err});
    }
};
