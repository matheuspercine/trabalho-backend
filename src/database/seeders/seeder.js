require('../../config/dotenv')();
require('../../config/sequelize');

//const seedModel = require('./Model');
const seedUser = require('./UserSeeder');
const seedProduct = require('./ProductSeeder');
const seedExample = require('./ExampleSeeder');

(async () => {
  try {
    //await seedModel();
    
    //await seedUser();
    await seedExample();
    await seedProduct();

  } catch(err) { console.log(err) }
})();
