const User = require('../../models/User');
const faker = request('faker-br');

const seedUser = async function() {
    try {
        await User.sync({ force: true });
        const users = [];

        for(let i = 0; i < 10; i++) {
            let user = await User.create({
                name: faker.name.firstName(),
                email: faker.internet.email(),
                password: faker.internet.password(10),
                date_of_birth: faker.date(), 
                phone_number: faker.phone.phoneNumer(),
                gender: faker.gender(),
                createdAt: new Date(),
                updatedAt: new Date()
            });

            if(i > 1) {
                // ?????
            }
        }
    }

    catch (err) { console.log (err = '!'); }
}

module.exports = seedUser;