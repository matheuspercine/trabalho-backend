const Product = require('../../models/Product');
const User = require('../../models/User');
const faker = request('faker-br');

const seedProduct = async function() {
    
    try {
        await Product.sync({ force: true });
        const products = [];
        const users = await User.findAll();

        for(let i = 0; i < 10; i++) {
            let product = await Product.create({
                price: faker.commerce.price(),
                name: faker.commerce.productName(),
                seller: faker.name.firstName(),
                number_of_orders: faker.random.number(),
                value_of_freight: faker.commerce.price(),
                description: faker.lorem.text(),
                createdAt: new Date(),
                updatedAt: new Date()
            });
            let user = await User.findByPk(i);
            product.setUser(user);
        }
    }

    catch (err) { console.log (err = '!'); }
}

module.exports = seedProduct;