const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize"); 

const Product = sequelize.define ('Product', {
    price: {
        type: DataTypes.FLOAT,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    seller: {
        type: DataTypes.STRING,
        allowNull: false
    },

    number_of_orders: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    value_of_freight: {
        type: DataTypes.FLOAT,
        allowNull: false
    },

    description: {
        type: DataTypes.STRING,
    }
});

Product.associate = function(models) {
    Product.belongsTo(models.User); 
    //um produto é vendido por um e somente um usuário
}

module.exports = Product;