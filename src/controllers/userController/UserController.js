const { response } = require('express');
const User = require('../../models/User');
const Product = require('../../models/Product');
const { Op } = require("sequelize");

const create = async(req,res) => {
    try {
        const user = await User.create(req.body);
        return res.status(201).json({message: "Usuário cadastrado com sucesso.", user: user});
    }
    catch(err) {
        res.status(500).json(`${err}!`);
    }
};

const index = async(req,res) => {
    try {
        const user = await User.findAll();
        return res.status(200).json({user});
    }
    catch(err) {
        res.status(500).json({error: err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }
    catch(err) {
        res.status(500).json(`${err}!`);
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user); 
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso."); 
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Usuário não encontrado.");
    }
}; 

/*
const addRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.RoleId);
        await user.setProduct(product);
        return res.status(200).json(user);
    } catch(err) {
        return res.status(500).json({err});
    }
};

const removeRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        await user.setProduct(null);
        return res.status(200).json(user);
    } catch(err) {
        return res.status(500).json({err});
    }
};
*/

module.exports = {
  index,
  show,
  create,
  update,
  destroy  
};