const getCadastroById = async (id) => {
    try {
      const response = await fetch(`http://localhost:3000/cadastrar/${id}`, {
        method: "GET",
      });
      const content = await response.json();
      return content;
    } catch (error) {
      console.log(error);
    }
  };
  
  export default getCadastroById;