import getCadastroById from "./getCadastrolById.js";

const updateBtn = document.querySelector("#list-button");
const inputName = document.querySelector("#cadastrar-name");
const inputEmail = document.querySelector("#cadastrar-email");
const inputPassword = document.querySelector("#cadastrar-password");
const inputDate = document.querySelector("#cadastrar-date");

const url = new URL(window.location);
const id = url.searchParams.get("id");

try {
  const { nome,email,password,date } = await getChoreById(id);
  inputTitle.value = title;
  inputDesc.value = description;

  inputName.value = nome;
  inputEmail.value = email;
  inputPassword.value = password;
  inputDate.value = date;
} catch (error) {
  console.log(error);
}

updateBtn.addEventListener("click", async (event) => {
  event.preventDefault();
  const nome = inputName.value;
  const email = inputEmail.value;
  const password = inputPassword.value;
  const date = inputDate.value;

  if (title === "" || desc === "") {
    console.log("preencha os campos");
  } else {
    await updateChore(title, desc);
    window.location.href = "../../public/index.html";
  }
});


const updateChore = async (nome,email,password,date) => {
  try {
    const response = await fetch(`http://localhost:3000/chores/${id}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: `${nome}`,
          email: `${email}`,
          password: `${password}`,
          date: `${date}`,
      }),
    });

    const content = await response.json();

    return content;
  } catch (error) {
    console.log(error);
  }
};