const postCadastro = async (nome,email,password,date) => {
    try {
      const response = await fetch("http://localhost:3000/cadastrar", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: `${nome}`,
          email: `${email}`,
          password: `${password}`,
          date: `${date}`,
        }),
      });
      const content = await response.json();
  
      return content;
    } catch (error) {
      console.log(error);
    }
  };

  export default postCadastro;