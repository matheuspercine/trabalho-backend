import postCadastro from "./postCadastro.js";

const inputName = document.querySelector("#cadastrar-name");
const inputEmail = document.querySelector("#cadastrar-email");
const inputPassword = document.querySelector("#cadastrar-password");
const inputDate = document.querySelector("#cadastrar-date");
const createBtn = document.querySelector("#cadastrar-button");

createBtn.addEventListener("click", async (event) => {
  event.preventDefault();
  const nome = inputName.value;
  const email = inputEmail.value;
  const password = inputPassword.value;
  const date = inputDate.value;
  await postCadastro(nome,email,password,date);
});