const deleteCadastro = async (id) => {
    try {
      const response = await fetch(`http://localhost:3000/cadastrar/${id}`, {
        method: "DELETE",
      });
      const content = await response.json();
      return content;
    } catch (error) {
      console.log(error);
    }
  };
  
  export default deleteCadastro;