const { response } = require('express');
const Product = require('../../models/Product');
const User = require('../../models/User');
const { Op } = require("sequelize");

const create = async(req,res) => {
    try {
        const product = await Product.create(req.body);
        return res.status(201).json({message: "Produto criado com sucesso.", product: product});
    }
    catch(err) {
        res.status(500).json({error: err});
    }
};

const index = async(req,res) => {
    try {
        const product = await Product.findAll();
        return res.status(200).json({product});
    }
    catch(err) {
        res.status(500).json({error: err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    }
    catch(err) {
        res.status(500).json({error: err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Product.update(req.body, {where: {id: id}});
        if(updated) {
            const product = await Product.findByPk(id);
            return res.status(200).send(product); 
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Produto não encontrado.");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Produto deletado com sucesso."); 
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Produto não encontrado.");
    }
}; 

const addRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await product.setUser(user);
        return res.status(200).json(product);
    } catch(err) {
        return res.status(500).json({err});
    }
};

const removeRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        await product.setUser(null);
        return res.status(200).json(product);
    } catch(err) {
        return res.status(500).json({err});
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationship,
    removeRelationship
  };