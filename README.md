# Tarefa Backend (TT 2022.1)

## Todos os métodos das controllers foram testados e funcionaram em minha máquina

## As seguintes pastas devem ser ignorados pois não fazem parte desta tarefa

- cadastroController (dentro de controllers, a qual está dentro de src)
- assets
- public
- style


## 💻 Rodando a tarefa na máquina

- para testar as rotas, os métodos das controllers e as models use o Postman
- para ver as tabelas de Usuário e Produto abra o arquivo database.sqlite com o SQLite
- para ver a modelagem, abra o arquivo Modelagem.brM3 com o brModelo


#### ➡️ Clone esse repositório

git clone https://gitlab.com/matheuspercine/formulario-civitas.git


#### ➡️ Entre na pasta do projeto

cd Tarefa Backend


#### ➡️ Abra o terminal (pode ser pelo VSCode) e execute o comando para instalar as dependências

- npm install dotenv
- npm install express
- npm install sequelize
- npm install node-dev
- npm install sqlite3


#### ➡️ Execute o comando para iniciar

- npm run migrate
- npm run dev

OBS: talvez seja necessário dar "npm run migrate", na primeira vez, para evitar problemas de não funcionamento de métodos da CRUD no Postman


#### ➡️ Para parar de servir a API, basta executar o comando `CTRL + C` no terminal

<hr>

<div align="center">
Copyright © 2022<br> 
  ✨ Created by <b>Matheus Percine</b> ✨
</div>
